#include <iostream>

using namespace std;

void inc_ref(int &num) {
	num ++;
}

int main() {
	int num1 = 1;
	cout << num1 << endl;
	inc_ref(num1);
	cout << num1 << endl;
	return 0;
}
