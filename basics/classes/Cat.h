#include <iostream>

class Cat {

private:
    std::string name;
    std::string hair_color;

    int lives_left;

public:
    Cat();
    Cat(std::string cat_name);
    Cat(std::string cat_name, std::string h_color);

    ~Cat();

    std::string getName();
    void setName(std::string cat_name);

    std::string getHairColor();
    void setHairColor(std::string h_color); // Poor cat :(

    int getLivesLeft();

    void meow();
};
