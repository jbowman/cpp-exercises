#include "Cat.h"
#include <iostream>

Cat::Cat() {
    this->name = "The Cat With No Name";
    this->hair_color = "Orange";
    this->lives_left = 9;
}

Cat::Cat(std::string cat_name) {
    this->name = cat_name;
    this->hair_color = "Orange";
    this->lives_left = 9;
}

Cat::Cat(std::string cat_name, std::string h_color) {
    this->name = cat_name;
    this->hair_color = h_color;
    this->lives_left = 9;
}

Cat::~Cat() {
    std::cout << "Meow! (Goodbye)" << std::endl;
}

std::string Cat::getName() {
    return this->name;
}

void Cat::setName(std::string cat_name) {
    this->name = cat_name;
}

std::string Cat::getHairColor() {
    return this->hair_color;
}

void Cat::setHairColor(std::string h_color) {
    this->hair_color = h_color;
}

int Cat::getLivesLeft() {
    return this->lives_left;
}

void Cat::meow() {
    std::cout << "Meeeeeeeeeow!" << std::endl;
}
